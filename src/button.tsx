import './button.css';

export function AppButton() {
    const handleSubmit = async (event:any) => {
        event.preventDefault();
        //@ts-expect-error foo
        window.electronAPI.sendMessage('Hello world');
        alert('Hello world');
    }

    return (
        <button onClick={handleSubmit}><h2>Hello from React!</h2></button>
    );
}
