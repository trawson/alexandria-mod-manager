import * as ReactDOMClient from "react-dom/client";
import { AppButton } from "./button";

function render() {
  const container = document.getElementById("app");
  const root = ReactDOMClient.createRoot(container);

  root.render(
    <AppButton/>
  );
}


render();
